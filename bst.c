#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// better way to use structs is with a typedef
typedef struct BST BST;

struct BST {
    int value;
    BST* left;
    BST* right;
};

void printBST(BST* node);
void freeBST(BST* root);
void freeChildren(BST* node);
BST* createNode(int value);
void addNode(BST* root, BST* node);

int main()
{
    BST* root = createNode(20);
    addNode(root, createNode(10));
    addNode(root, createNode(25));
    addNode(root, createNode(5)); 

    printBST(root);
    printf("\n");
    // (20 (10 (5 X, X), X), (25 X, X))                                                                                                   


    freeBST(root);
    // free on 5                                                                                                                          
    // free on 10                                                                                                                         
    // free on 25                                                                                                                         
    // free on 20

    return 0;
}

void printBST(BST* node) 
{
    if(node) {
        printf("(%d ", node->value);
        printBST(node->left);
        printf(", ");
        printBST(node->right);
        printf(")");
    }else {
        printf("X");
    }
}

void freeBST(BST* root)
{
    freeChildren(root);     
    printf("free on %d\n", root->value);
    free(root);
}

void freeChildren(BST* node) 
{
    if(node->left) {
        freeChildren(node->left);
        printf("free on %d\n", node->left->value);
        free(node->left);
        
    }
    if(node->right) {
        freeChildren(node->right);
        printf("free on %d\n", node->right->value);
        free(node->right);
    }
}

BST* createNode(int value)
{
    BST* node = malloc(sizeof(BST));
    node->value = value;
    node->left = NULL; //you'll get a segfault if you don't null these out before you read them
    node->right = NULL;

    return node;
}

void addNode(BST* root, BST* node) 
{
    if(node->value <= root->value) 
    {
        if(root->left) 
        {
            addNode(root->left, node);
        }
        else
        {
            root->left = node;
        }
    }
    else
    {
        if(root->right) 
        {
            addNode(root->right, node);
        }
        else
        {
            root->right = node;
        }
    }
}