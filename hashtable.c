#include <stdio.h>
#include <stdlib.h>

typedef struct Person Person;
typedef struct HashEntry HashEntry;
typedef struct HashTable HashTable;

struct Person {
    char name[30]; // 30 'spaces' for characters
    int age; // simple value
};
struct HashEntry {
    int count; // number of people in this entry (chained)
    Person* people; // array of people
};
struct HashTable {
    int capacity; // number of entries in this table
    HashEntry* entries; //array of entries
};

HashTable* createHashTable();

int main()
{
    HashTable* t = createHashTable();
    printf("capacity of hashtable %d\n", t->capacity);

    return 0;
}

HashTable* createHashTable() {
    // allocate memory for the table then spaces to hold entries
    HashTable* t = malloc(sizeof(HashTable));
    t->capacity = 5;
    t->entries = malloc(sizeof(HashEntry) * 5); //enough space for 5 entries
    
    // set each entry to nothing since hashtable is currently empty
    int i;
    for(i=0; i < (t->capacity); i++) {
        t->entries[i].count = 0; // no people in this entry
        t->entries[i].people = NULL; // person array doesn't exist yet'
    }

    // return a pointer to this table
    return t; 
}

void addPersonToHashTable(HashTable* table, Person* person) {
    // calculate hashcode
    // entry index = hashcode % (table->size)
    // increase count in entry by 1
    // create a new array to hold all the previous people plus this new one (count)
    // copy old array into new, add new person at the end 
    // set t->entries[index].people = new array
    // NOTE: array doesn't really hold a Person, it holds a pointer to a Person
}