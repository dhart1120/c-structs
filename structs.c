#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// better way to use structs is with a typedef
typedef struct Person Person;

struct Person {
    char name[30];
    int age;
};

int main()
{
    // Using a struct as a value type 
    Person dustin;
    dustin.age = 29;
    strcpy(dustin.name, "Dustin");

    printf("%s is %d years old\n", dustin.name, dustin.age);


    // Using a pointer to a struct
    Person* allison = malloc(sizeof(Person)); // must allocate enough memory to hold a Person stuct. Memory is allocated from the heap.
    allison->age = 26; // must "dereference" the pointer to access it's fields'
    strcpy(allison->name, "Allison");
    
    printf("%s is %d years old\n", allison->name, allison->age);

    return 0;
}